# Common operations on project.
.PHONY: develop public clean dist-clean maintainer-clean

MAKEFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
PROJECT_DIR := $(patsubst %/,%,$(dir $(MAKEFILE_PATH)))
PROJECT_NAME := $(notdir $(PROJECT_DIR))
BUILD_DIR := $(PROJECT_DIR)/public

YARN ?= yarn
PARCEL ?= $(YARN) run parcel


#: develop - Install development libraries.
develop:
	$(YARN) install


#: public - Generate public/ folder contents.
public:
	$(PARCEL) build --no-content-hash --dist-dir=$(BUILD_DIR) assets/index.html
	#cp -r assets/img public/
	#cp -r assets/files public/


#: serve - Serve public/ folder on localhost:1234
serve:
	$(PARCEL) serve --dist-dir=$(BUILD_DIR) assets/index.html


#: clean - Basic cleanup, mostly temporary files.
clean:


#: distclean - Remove local builds
dist-clean: clean
	rm -rf public/


#: maintainer-clean - Remove almost everything that can be re-generated.
maintainer-clean: dist-clean
	rm -rf .yarn*
