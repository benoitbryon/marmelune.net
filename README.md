# marmelune.net

This is the theme for http://www.marmelune.net/ website.

* Requires Yarn from NodeJS, Git
* Clone the repository: git clone git@github.com:benoitbryon/marmelune.net.git
* Go to repository folder: cd marmelune.net
* Generate deploy builder utilities: make develop
* Generate public files: make public
* Run local HTTP server to check generated files: make serve
* Open generated files: firefox http://localhost:8000/
